var AWS = require('aws-sdk');
var config = require('config');
var Promise = require('bluebird');
var multiline = require('multiline');

AWS.config.update(config.aws);

var ses = Promise.promisifyAll(new AWS.SES());

var params = {
  Destination: {
    // sandboxではverifyしたメールアドレスのみ
    ToAddresses: [
      "p.baleine@gmail.com",
      "adgj6565@gmail.com"
    ]
  },
  Message: {
    Body: {
      Html: {
        Data: multiline(function() {/*
<h1 style="color: cyan">
  Hello, SES from aws-sdk.
</h1>
<p>
  日本語もいけるよね？
</p>
        */}),
        Charset: 'UTF-8'
      },
      Text: {
        Data: 'Hello, SES from aws-sdk.',
        Charset: 'UTF-8'
      }
    },
    Subject: {
      Data: 'SES test mail',
      Charset: 'UTF-8'
    }
  },
  Source: 'p.baleine@gmail.com',
  ReturnPath: 'p.baleine@gmail.com'
};

console.log('send email...');

ses.sendEmailAsync(params)
  .then(function(data) {
    console.log('success');
    console.log(data);
  })
  .catch(function(e) {
    console.log('fail: %j', e);
  });
